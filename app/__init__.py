from dutyroster2ical import DutyCalendar
from dutyroster2ical.confluence_downloader import ConfluenceDownloader
from webserver import Webserver
from configparser import ConfigParser


CONFIG_FILE = 'config.ini'


def main():
    parser = ConfigParser()

    with open(CONFIG_FILE) as file:
        parser.read_file(file)

    port = int(parser["webserver"]["port"])

    url = parser["confluence"]["url"]
    username = parser["confluence"]["username"]
    password = parser["confluence"]["password"]

    downloader = ConfluenceDownloader(url, username, password, True)
    calendar = DutyCalendar(downloader)
    calendar.update()

    webserver = Webserver(calendar, port)
    webserver.start()
