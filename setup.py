from setuptools import setup, find_packages

setup(
    name="dutyroster2ical",
    version="0.1",
    author="Niko Riipiranta",
    description="Create an ical file from Duty roster Confluence page",
    packages=find_packages(),
    install_requires=[
        "requests",
        "lxml",
        "ics",
        "pyramid",
        "waitress",
        "pytz",
    ],
    entry_points={
        "console_scripts": [
            'dutyroster2ical = app:main'
        ]
    }
)
